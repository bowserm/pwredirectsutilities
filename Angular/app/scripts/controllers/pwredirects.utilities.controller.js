(function() {
    "use strict";
	
	function PwRedirectsUtilitiesController($scope, redirectUrlsResource, pwRedirectsUtilitiesResource, notificationsService, dialogService, entityResource, iconHelper) {

		//Property Editor has loaded...
		console.log('Hello from PwRedirectsUtilitiesController');
		
		var vm = this;

		vm.dashboard = {
			searchTerm: "",
			loading: false,
			urlTrackerDisabled: false,
			userIsAdmin: false
		};
		
		vm.adding = {
			isAdding: false,
			originalUrl: "",
			redirectedTo: "",
			hasError: false,
			success: false,
			errorMessage: ""
		};

        vm.pagination = {
            pageIndex: 0,
            pageNumber: 1,
            totalPages: 1,
            pageSize: 20
        };


		
		function checkEnabled() {
			vm.dashboard.loading = true;
			return redirectUrlsResource.getEnableState().then(function (response) {
				vm.dashboard.urlTrackerDisabled = response.enabled !== true;
				vm.dashboard.userIsAdmin = response.userIsAdmin;
				vm.dashboard.loading = false;
			});
		}


        function search() {

            vm.dashboard.loading = true;

            var searchTerm = vm.dashboard.searchTerm;
            if (searchTerm === undefined) {
                searchTerm = "";
            }

            redirectUrlsResource.searchRedirectUrls(searchTerm, vm.pagination.pageIndex, vm.pagination.pageSize).then(function(response) {

                vm.redirectUrls = response.searchResults;

                // update pagination
                vm.pagination.pageIndex = response.currentPage;
                vm.pagination.pageNumber = response.currentPage + 1;
                vm.pagination.totalPages = response.pageCount;

                vm.dashboard.loading = false;

            });
        }

		$scope.addRedirect = function() {
            vm.adding.isAdding = true;
			vm.adding.success = false;
        };
		$scope.saveRedirect = function() {
			vm.dashboard.loading = true;
			vm.dashboard.searchTerm = "";

			if (/^https?:\/\//.test(vm.adding.originalUrl)) {
				vm.adding.hasError = true;
				vm.adding.errorMessage = "Only relative URLs supported.  Please remove http / https and domain.";
				vm.dashboard.loading = false;
				return false;
			}

			var parsedUrl = vm.adding.originalUrl.replace (
								new RegExp("(.*)/$"),
								"$1");

			pwRedirectsUtilitiesResource.addRedirectUrl(parsedUrl,vm.adding.redirectedTo).then(function(response) {
				vm.dashboard.loading = false;
				vm.adding.isAdding = false;
				search();
				vm.adding.originalUrl = "";
				vm.adding.hasError = false;
				vm.adding.success = true;
				$scope.clear();
			}, function(reason) {
				vm.adding.hasError = true;
				vm.adding.errorMessage = "An issue occurred when trying to add a Redirect.  Please verify the Original Url is a relative url.";
				vm.dashboard.loading = false;
				return false;
			});
        };
		$scope.cancelAddRedirect = function() {
            vm.adding.isAdding = false;
			vm.adding.originalUrl = "";
			vm.dashboard.searchTerm = "";
			$scope.clear();

        };
		$scope.checkSimilarRedirects = function() {
            vm.dashboard.searchTerm = vm.adding.originalUrl;
			search();
        };
		
		
		/// For content picker stuff.  May want to refactor
	    if (!$scope.model) {
	        $scope.model = {};
	    }
	    if (!$scope.model.value) {
	        $scope.model.value = {
	            type: "content"
	        };
	    }
		
		if($scope.model.value.id && $scope.model.value.type !== "member"){
			var ent = "Document";
			
			entityResource.getById($scope.model.value.id, ent).then(function(item){
				item.icon = iconHelper.convertFromLegacyIcon(item.icon);
				$scope.node = item;
			});
		}

		$scope.clear = function() {
			$scope.model.value.id = undefined;
			$scope.node = undefined;
			$scope.model.value.query = undefined;
			vm.adding.redirectedTo = "";
		};


		function populate(item){
				$scope.clear();
				item.icon = iconHelper.convertFromLegacyIcon(item.icon);
				$scope.node = item;
				$scope.model.value.id = item.id;
				vm.adding.redirectedTo = item.key;
		}

		$scope.openContentPicker = function(){
			$scope.treePickerOverlay = {
				view: "treepicker",
				section: $scope.model.value.type,
				treeAlias: $scope.model.value.type,
				multiPicker: false,
				show: true,
				submit: function(model) {
					var item = model.selection[0];
					populate(item);
					$scope.treePickerOverlay.show = false;
					$scope.treePickerOverlay = null;
				}
			};
		};
		

		function init() {
			checkEnabled().then(function() {
				search();
			});
		}

		init();

	}

	angular.module('umbraco').controller('PwRedirectsUtilitiesController', PwRedirectsUtilitiesController);

})();