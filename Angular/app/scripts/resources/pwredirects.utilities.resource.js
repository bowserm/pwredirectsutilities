/**
 * @ngdoc service
 * @name umbraco.resources.PwRedirectsUtilitiesResource
 * @function
 *
 * @description
 * Used by the PwRedirectsUtilities Dashboard to add redirects.
 */
(function() {
    'use strict';

    function PwRedirectsUtilitiesResource($http, umbRequestHelper) {

        /**
         * @ngdoc function
         * @name umbraco.resources.pwRedirectsUtilitiesResource#addRedirectUrl
         * @methodOf umbraco.resources.pwRedirectsUtilitiesResource
         * @function
         *
         * @description
         * Called to add a redirect
         * ##usage
         * <pre>
         * umbraco.resources.pwRedirectsUtilitiesResource.addRedirectUrl('/test/', '24897E84-0310-4420-AE43-20568B986D7D')
         *    .then(function() {
         *
         *    });
         * </pre>
         * @param {String} originalUrl url of the location to redirect from
         * @param {String} contentKey Key (guid) of the Content to redirect to
         */
        function addRedirectUrl(originalUrl, contentKey) {
            return umbRequestHelper.resourcePromise(
                $http({
                    method: 'GET',
                    url: '/Umbraco/backoffice/Api/PwRedirectsUtilities/AddRedirectUrl',
                    params: { originalUrl: originalUrl,
                              contentKey: contentKey}
                }),
                'Failed to add redirect');
        }

        var resource = {
            addRedirectUrl: addRedirectUrl
        };

        return resource;

    }

    angular.module('umbraco.resources').factory('pwRedirectsUtilitiesResource', PwRedirectsUtilitiesResource);

})();
